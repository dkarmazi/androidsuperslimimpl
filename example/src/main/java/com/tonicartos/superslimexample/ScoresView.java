package com.tonicartos.superslimexample;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ScoresView extends FrameLayout {
    private TextView sportName;

    public ScoresView(Context context) {
        super(context);
        init();
    }

    public ScoresView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScoresView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        inflate(getContext().getApplicationContext(), R.layout.scores_view, this);

        sportName = (TextView) findViewById(R.id.sportName);
    }

    public void changeView() {
        ViewGroup.LayoutParams lp = sportName.getLayoutParams();
        lp.width = 800;
        lp.height = 200;
        sportName.setLayoutParams(lp);
        sportName.setText("SCORES DELAYED SETUP");
        sportName.setBackgroundColor(getResources().getColor(R.color.primary_dark));
    }
}
