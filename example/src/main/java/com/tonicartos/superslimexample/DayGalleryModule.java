package com.tonicartos.superslimexample;

import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

public class DayGalleryModule {
    private GalleryView galleryView;

    public DayGalleryModule() {}

    public View getView(ViewGroup container) {
        if(galleryView == null){
            galleryView = new GalleryView(container.getContext());

            List<String> list = new ArrayList<String>();
            list.add("One");
            list.add("Two");
            list.add("Three");
            list.add("Four");
            list.add("Five");

            GalleryView.GalleryPagerAdapter galleryPagerAdapter = new GalleryView.GalleryPagerAdapter(list);

            galleryView.setAdapter(galleryPagerAdapter);
        }

        return galleryView;
    }
}
