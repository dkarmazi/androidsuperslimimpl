package com.tonicartos.superslimexample;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tonicartos.superslim.LayoutManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CountryNamesAdapter extends RecyclerView.Adapter {
    private static final String TAG = "dkarmazi-dkarmazi";
    private static final int VIEW_TYPE_HEADER_FIRST = 1;
    private static final int VIEW_TYPE_HEADER = 2;
    private static final int VIEW_TYPE_CONTENT = 3;
    private static final int VIEW_TYPE_GALLERY = 4;
    private static final int VIEW_TYPE_REPEATED = 5;
    private static final int VIEW_TYPE_SCORES = 6;

    private static final int GALLERY_POSITION = 50;

    private final ArrayList<LineItem> mItems;
    private Context context;
    private ViewPager pager;

    private List<View> pendingPool = new ArrayList<>();
    private boolean isLayoutInProgress = false;

    List<String> list = getSampleList();
    final GalleryView.GalleryPagerAdapter galleryPagerAdapter = new GalleryView.GalleryPagerAdapter(list);


    public CountryNamesAdapter(Context context) {
        final String[] countryNames = context.getResources().getStringArray(R.array.country_names);

        mItems = new ArrayList<>();

        //Insert headers into list of items.
        String lastHeader = "";
        int headerCount = 0;
        int sectionFirstPosition = 0;

        for (int i = 0; i < countryNames.length; i++) {
            String header = countryNames[i].substring(0, 1);
            if (!TextUtils.equals(lastHeader, header)) {
                // Insert new header view and update section data.
                sectionFirstPosition = i + headerCount;
                lastHeader = header;
                headerCount += 1;
                mItems.add(new LineItem(header, true, sectionFirstPosition));
            }
            mItems.add(new LineItem(countryNames[i], false, sectionFirstPosition));
        }

        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // Log.d("dkarmazi-dkarmazi", "onCreateViewHolder: " + viewType);

        if (viewType == VIEW_TYPE_HEADER) {
            int layoutId = R.layout.header_item;
            View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            return new CountryViewHolder(view);

        } else if (viewType == VIEW_TYPE_HEADER_FIRST) {
            int layoutId = R.layout.header_first_item;
            View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            return new CountryViewHolder(view);

        } else if (viewType == VIEW_TYPE_GALLERY) {
            View v = LayoutInflater.from(context).inflate(R.layout.gallery_pager, parent, false);

            pager = (ViewPager) v.findViewById(R.id.gallery_pager);

            pager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    pager.setAdapter(galleryPagerAdapter);
                }
        }, 200);



            return new CountryViewHolder(v);
        } else if(viewType == VIEW_TYPE_SCORES) {
            final ScoresView scoresView = new ScoresView(context);

            scoresView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scoresView.changeView();

                    if(isLayoutInProgress) {
                        Log.d(TAG, "ADDED TO POOL");
                        pendingPool.add(scoresView);
                    } else {
                        Log.d(TAG, "EXECUTED IMMEDIATELY");
                        scoresView.requestLayout();
                    }
                }
            },200);

            return new CountryViewHolder(scoresView);
        } else if (viewType == VIEW_TYPE_REPEATED) {
            Log.d(TAG, "onCreateViewHolder: PROBLEM: AFTER FIRST");

            int layoutId = R.layout.text_line_item_two;
            View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            return new CountryViewHolder(view);
        } else {
            int layoutId = R.layout.text_line_item;
            View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            return new CountryViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
//        Log.d(TAG, "onBindViewHolder: position: " + position);

        final LineItem item = mItems.get(position);
        final View itemView = holder.itemView;

        if(holder instanceof CountryViewHolder) {
            ((CountryViewHolder) holder).bindItem(item.text);
        } else if(holder instanceof CountryTwoViewHolder) {
            ((CountryTwoViewHolder) holder).bindItem(item.text);
        }

        if(position == 25) {
            final ScoresView v = (ScoresView) holder.itemView;

//            v.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Log.d(TAG, "run: REQUEST LAYOUT");
////                    v.requestLayout();
//                }
//            }, 200);


        } else if(position == GALLERY_POSITION) {
            final View viewPager = holder.itemView;


//            if(isLayoutInProgress) {
//                pendingPool.add(viewPager);
//            } else {
//                viewPager.requestLayout();
//            }

            viewPager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewPager.requestLayout();
                }
            }, 3000);
        }

        // Overrides xml attrs, could use different layouts too.
        final LayoutManager.LayoutParams lp = LayoutManager.LayoutParams.from(itemView.getLayoutParams());

        lp.setSlm("myCustomManager");
        lp.setFirstPosition(item.sectionFirstPosition);
        itemView.setLayoutParams(lp);
    }

    @Override
    public int getItemViewType(int position) {
        int type = VIEW_TYPE_CONTENT;

        if(position == 1) {
            return VIEW_TYPE_REPEATED;
        }

        if(position == 25) {
            type = VIEW_TYPE_SCORES;
        } else if(position == GALLERY_POSITION) {
            type = VIEW_TYPE_GALLERY;
        } else if(mItems.get(position).isHeader) {
            if(position == 0) {
                type = VIEW_TYPE_HEADER_FIRST;
            } else {
                type = VIEW_TYPE_HEADER;
            }
        }

        return type;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }



    private static class LineItem {
        public int sectionFirstPosition;
        public boolean isHeader;
        public String text;

        public LineItem(String text, boolean isHeader, int sectionFirstPosition) {
            this.isHeader = isHeader;
            this.text = text;
            this.sectionFirstPosition = sectionFirstPosition;
        }
    }

    private static List<String> getSampleList() {
        List<String> list = new ArrayList<String>();
        list.add("One");
        list.add("Two");
        list.add("Three");
        list.add("Four");
        list.add("Five");

        return list;
    }


    public void setLayoutInProgress(boolean layoutInProgress) {
        Log.d(TAG, "setLayoutInProgress: " + layoutInProgress);

        isLayoutInProgress = layoutInProgress;
    }

    public void executePendingLayoutRequests() {
        Log.d(TAG, "executePendingLayoutRequests: ");
        Iterator<View> iterator = pendingPool.iterator();

        while(iterator.hasNext()) {
            View v = iterator.next();

            if(v != null) {
                Log.d(TAG, "FOR: executePendingLayoutRequests: ");

                v.requestLayout();

                pendingPool.remove(v);
            }
        }
    }
}
