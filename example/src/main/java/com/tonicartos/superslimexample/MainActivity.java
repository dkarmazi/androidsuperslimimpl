package com.tonicartos.superslimexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;


public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        final CountryNamesAdapter countryNamesAdapter = new CountryNamesAdapter(this);


        CustomLayoutManager lm = new CustomLayoutManager(this);
        lm.setCustomLayoutManagerListener(new CustomLayoutManager.CustomLayoutManagerListener() {
            @Override
            public void onChildrenStart() {
                countryNamesAdapter.setLayoutInProgress(true);
            }

            @Override
            public void onChildrenEnd() {
                countryNamesAdapter.setLayoutInProgress(false);
                countryNamesAdapter.executePendingLayoutRequests();
            }
        });

        lm.addSlm("myCustomManager", new LinearSlmCustom(lm));
        recyclerView.setLayoutManager(lm);

        recyclerView.setAdapter(countryNamesAdapter);
    }
}
