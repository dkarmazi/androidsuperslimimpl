package com.tonicartos.superslimexample;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


public class MyBehavior extends CoordinatorLayout.Behavior<View> {
//    private static final String TAG = "dkarmazi-dkarmazi";

    private View firstHeaderView = null;

    public MyBehavior() {}

    public MyBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, final View child, final View dependency) {
        if(dependency instanceof AppBarLayout) {

            RecyclerView recyclerView = (RecyclerView) parent.findViewById(R.id.recycler_view);
            firstHeaderView = recyclerView.findViewById(R.id.header_first_item);

//            Log.d(TAG, "layoutDependsOn: " + recyclerView.getTop());


            return true;
        }

        return false;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
//        Log.d(TAG, "onDependentViewChanged: " + dependency.getBottom());

        if(firstHeaderView != null) {
            float actualBottom = dependency.getBottom() - 72f;
            float absBottom = 240f - 72f;

            firstHeaderView.setAlpha(1 - actualBottom / absBottom);

        }

        return super.onDependentViewChanged(parent, child, dependency);
    }
}
