package com.tonicartos.superslimexample;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.List;

public class GalleryView extends FrameLayout {
    public ViewPager pager;

    public GalleryView(Context context) {
        super(context);
        init();
    }

    public GalleryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GalleryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GalleryView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.gallery_pager, this, true);
        pager = (ViewPager) findViewById(R.id.gallery_pager);
        pager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin));
    }

    public void setAdapter(GalleryPagerAdapter adapter){
        pager.setAdapter(adapter);
    }

    public static class GalleryPagerAdapter extends PagerAdapter {
        private final List<String> list;

        public GalleryPagerAdapter(final List<String> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            GallerySlide slide = new GallerySlide(container.getContext());
            slide.setData(list.get(position));
            container.addView(slide);
            return slide;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public float getPageWidth(int position) {
            return 1F;
        }
    }
}
