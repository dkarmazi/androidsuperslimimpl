package com.tonicartos.superslimexample;

import android.util.Log;

import com.tonicartos.superslim.LayoutManager;
import com.tonicartos.superslim.LayoutState;
import com.tonicartos.superslim.LinearSLM;
import com.tonicartos.superslim.SectionData;

public class LinearSlmCustom extends LinearSLM {
    public LinearSlmCustom(LayoutManager layoutManager) {
        super(layoutManager);
    }

    @Override
    public int computeHeaderOffset(int firstVisiblePosition, SectionData sd, LayoutState state) {
//        int offset = super.computeHeaderOffset(firstVisiblePosition, sd, state);
//        Log.d("dkarmazi-dkarmazi-2", "computeHeaderOffset: " + offset);
        return 1;
    }
}
