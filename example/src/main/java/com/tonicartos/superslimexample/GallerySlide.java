package com.tonicartos.superslimexample;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;


public class GallerySlide extends FrameLayout {

    private TextView title;

    public GallerySlide(Context context) {
        super(context);
        init();
    }

    public GallerySlide(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GallerySlide(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GallerySlide(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        LayoutInflater.from(getContext()).inflate(R.layout.gallery_slide, this, true);

        title = (TextView) findViewById(R.id.gallery_title);
    }

    public void setData(String s) {
        title.setText(s);
    }
}
