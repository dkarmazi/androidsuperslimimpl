package com.tonicartos.superslimexample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.tonicartos.superslim.LayoutManager;

public class CustomLayoutManager extends LayoutManager {
    private CustomLayoutManagerListener customLayoutManagerListener;

    public CustomLayoutManager(Context context) {
        super(context);
    }



    public void setCustomLayoutManagerListener(CustomLayoutManagerListener customLayoutManagerListener) {
        this.customLayoutManagerListener = customLayoutManagerListener;
    }

    public interface CustomLayoutManagerListener {
        void onChildrenStart();
        void onChildrenEnd();
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        if(customLayoutManagerListener != null) {
            customLayoutManagerListener.onChildrenStart();
        }

        super.onLayoutChildren(recycler, state);

        if(customLayoutManagerListener != null) {
            customLayoutManagerListener.onChildrenEnd();
        }
    }
}
